# Weighted Simplicial Homology version 1.1.1.a1

## Table of Contents
--- 
1. [Computing Weighted Simplicial Homology with Python](##Computing-Weighted-Simplicial-Homology-With-Python)
2. [How to Install Package](##How-To-Install-Package)
3. [Run](##Run)
4. [Examples](##Examples)
5. [Contact me](##Contact-me)
6. [References](##References)
7. [Upcoming Features](##Upcoming-Features)
## Computing Weighted Simplicial Homology with Python
---
This python package ingests a (monotone) weighted simplicial complex from a file 
and computes its weighted homology over the ring of power series over the rational numbers ($`R = \mathbb{Q}[[\pi]]`$, with uniformizer $`\pi`$.).
Weighted simplicial homology is a deformation of classical simplicial 
homology for which now the boundary operator takes into account simplex weights - see 
[this paper](https://www.mdpi.com/2227-7390/9/7/744) for a discussion of the theory of weighted complexes and their homology, as well as
applications to the study of RNA bi-structures. If the weighted simplicial complex is trivially
weighted (all of the weights are identical) then the package returns 
```math
H_{n}(X, \mathbb{Q}) \otimes R.
```

A naive attempt at this computation might proceed via Smith normalization directly on the weighted 
boundary matrix - this would be a matrix whose entries are taken from a discrete 
valuation ring (DVR); for instance, rational polynomials, localized at the prime ideal $`(0)`$. 
This approach proves quickly intractable due to the storing and processing of the representations 
of valuation ring elements
and the implementation of their algebra. The Smith normalization process (already susceptible to coefficient balooning) becomes too
unwieldy, even for the smallest of complexes (on the order of ten simplices!). 

The methodology implemented in this package is a novel approach we put forward in an upcoming paper.
By changing the DVR model to the rational power series ring, we prove we can reduce the computation 
to performing linear algebra over the residue field of rationals $`\mathbb{Q}`$. This both circumvents the tendency
of Smith normalization coefficients to balloon, as well as allowing the use of extant linear algebra functionalities 
present in numpy. Our approach, as well as a general discussion 
of monotone weighted simplicial complexes and their homology over a rational power series ring will be presented in more detail in an upcoming paper, and a link to the arxiv preprint will be available here. 

## How To Install Package
---
`WeightedSimplicialHomology` will be available on PyPI and installable via `pip`. Use the command:

`python -m pip install WeightedSimplicialHomology`

## Run
---
Once installed, the weighted simplicial homology package can be imported for use in python scripts. 

The module `WeightedSimplicialHomology.Simplicial_Constructors` provides utilities for constructing weighted simplicial complexes. The first option is to provide input from a file. To read in input from a file, we use the 
`Simplicial_File_Reader` class: we first instantiate `Simplicial_File_Reader` with the path to our input file 

`my_reader = WeightedSimplicialHomology.Simplicial_Constructors.Simplicial_File_Reader("/path/to/file")`.

Then, we build the complex:

`my_complex = my_reader.make_complex_from_file()`.


Each line in the file should specify a simplex by giving its vertex list
and its weight; the vertex list should be a comma separated list followed by a semicolon, and the weight
should be given after the semicolon. For example:

`1,2,3;4`

specifies the simplex `[1,2,3]` with weight `4`. It is assumed at present that the user's file specifies a 
valid simplicial complex, i.e that every face of a simplex is also in the simplicial complex, so special 
care should be taken to ensure that a valid simplicial structure is specified. The vertices should be specified 
in the *simplicial order* - this order need not necessarily be the standard numerical order on the labels, but it
should be consistent throughout the complex: if a vertex with label `a` appears before a vertex with label 
`b` in one simplex, then `a` should appear before `b` in all other simplices as well. See the [Examples](##Examples)
section for a demonstration of this. It is recommended to use file input and the 
`Simplicial_File_Reader` class to construct weighted simplicial complexes.

If one wishes to build a simplicial complex that is trivially weighted 
(that is, the weight of each simplex is the same), then one can use 
the `generate_complex_using_max_simplices` method of `Simplicial_Constructors`, which allows a user to specify a simplicial complex by specifying its *maximal simplices* (a simplex $`\sigma`$ is maximal if, for any simplex $`\tau`$ such that $`\sigma`$ is a face of $`\tau`$, we must have $`\sigma = \tau`$). `generate_complex_using_max_simplices` takes 
a list of maximal_simplices, which are specified as lists of vertex labels, and optionally takes a positive integer weight (which defaults to $`1`$). As before, the order of vertex labels in the simplices should be 
consistent. 

For a module $`M`$ over a principal ideal domain $`R`$ (like $`\mathbb{Q}[[\pi]]`$) we have: 

```math
M \cong R^{l} \oplus \left(\bigoplus_{k = 1}^{n}R/(p_{k}^{d_{k}})\right).
```

where the $`p_{k}`$ are (not necessarily distinct) prime elements of $`R`$. 
The powers $`d_{k}`$ are typically taken to be positive integers, in which 
case this decomposition is known as the *elementary divisors form* of $`M`$.

In our case where $`R = \mathbb{Q}[[\pi]]`$, there is only one prime element: $`\pi`$. Hence, the ideals $`(p_{k}^{d_{k}})`$ are all of the form 
$`(\pi^{d_{k}})`$. Since there is only one prime, the elementary divisors 
form for $`M`$ coincides with another canonical form for $`M`$ - the 
*invariant factors form* (if the $`d_{k}`$ are arranged in increasing order).

The method `mWSC.get_Homology` will return the weighted homology in 
dimension $`n`$ in the form `(free_rank, torsion_coefficients)`. 
`free_rank` is a nonnegative integer which corresponds to $`l`$ in the 
decomposition above. `torsion_coefficients` is a list of *nonnegative* integers *corresponding to the $`d_{k}`$*. We emphasize that we are loosening the restriction that the $`d_{k}`$ be positive, and that the coefficients correspond to the powers - hence a "torsion coefficient" of 
$`0`$ corresponds to the submodule $`R/(1)`$ (which is the zero module), a 
torsion coefficient of $`1`$ corresponds to the submodule $`R/(\pi^{1})`$, and so on.

The method `mWSC.print_Homology` is intended to make the output of 
`get_Homology` more human readable. `print_Homology` returns a string formatted in the style of the decomposition of $M$ provided above. See the
[Examples](##Examples) section for sample outputs under `get_Homology` 
and `print_Homology`. 

## Examples
---

### Sphere

Input code:
```
sphere = WeightedSimplicialHomology.Simplicial_Constructors.generate_complex_using_max_simplices([
    [0,1,2],
    [0,2,3],
    [1,2,3],
    [0,1,3]
]) 
sphere.get_Homology(0) # compute the homology of the sphere in dimension 0 (should be free with rank 1)
```

Output:
```
(1, [0, 0, 0])
```

We may also use the `print_Homology` method

```
sphere.print_Homology(0)
```

Output:
```
Q[[x]]^1
```

### Torus

Input:
```
torus = WeightedSimplicialHomology.Simplicial_Constructors.generate_complex_using_max_simplices([
[0,1,3],
[1,3,5],
[1,5,2],
[5,2,6],
[2,6,0],
[6,0,3],
[3,4,5],
[4,5,7],
[5,7,6],
[7,6,8],
[6,8,3],
[8,3,4],
[4,0,7],
[0,7,1],
[7,1,8],
[1,8,2],
[8,2,4],
[2,4,0]
])

torus.get_Homology(1) #get homology of torus in dimenion 1 (should be free with rank 2)
```
Output:
```
(2, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
```

With `print_Homology()`

```
torus.print_Homology(1)
```

```
Q[[x]]^2
```

### $`RP^{2}`$

Input: 
```
rp2 = WeightedSimplicialHomology.Simplicial_Constructors.generate_complex_using_max_simplices([
    [0,1,8],
    [0,8,7],
    [0,5,7],
    [5,4,7],
    [4,6,7],
    [6,8,7],
    [3,4,6],
    [2,1,8],
    [2,6,8],
    [3,2,6]
])

rp2.get_Homology(1) #get homology of RP2 in the first dimension (should be 0)
```
Output:
```
(0, [0, 0, 0, 0, 0, 0, 0, 0, 0, 0])
```

With `print_Homology()`:

```
Q[[x]]^0
```

We take the convention that $`R^{0}`$ will be the trivial module over $`R`$. 

These results are expected as the complexes are trivially weighted, and so
the classical homology is reproduced.

### Weighted triangles joined by an edge
We construct this weighted complex with the following file input:
```
0; 4
1; 5
2; 5
3; 4
0,1; 3
0,2; 2
1,2; 4
1,3; 3
2,3; 2
0,1,2; 1
```
in `weighted_joined_triangles.txt` by calling:
```
from WeightedSimplicialHomology import mWSC as mWSC
from WeightedSimplicialHomology.Simplicial_Constructors import Simplicial_File_Reader

test_reader = Simplicial_File_Reader("weighted_joined_triangles.txt")

triangles_complex = test_reader.make_complex_from_file()

print(triangles_complex.get_Homology(0))
print(triangles_complex.get_Homology(1))
print(triangles_complex.get_Homology(2))
```

Output:
```
(1, [1, 1, 1])
(1, [1])
(0, [])
```

Of note here is the presence of nontrivial weights which lead to arithmetic torsion in dimensions $`0`$ and $`1`$.
## Contact me
---
This project was developed by Neelav Dutta, working with the Mathematical Biocomplexity division 
of the Biocomplexity Institute at the University of Virginia. Questions and comments on this 
project can be sent to 

nsd8uc@virginia.edu.

## References
---

Andrei C. Bura, Qijun He, Christian M. Reidys,
Loop homology of bi-secondary structures,
Discrete Mathematics,
Volume 344, Issue 6,
2021,
112371,
ISSN 0012-365X,
https://doi.org/10.1016/j.disc.2021.112371.
(https://www.sciencedirect.com/science/article/pii/S0012365X21000844)



## Upcoming Features: 
---
- Parallelization of processes not handled by 'numpy.linalg' using ctypes.
- Extension of functionality to computing weighted homology over the power series ring with coefficients in finite fields.

