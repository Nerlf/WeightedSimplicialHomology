from WeightedSimplicialHomology import mWSC as mWSC

class Simplicial_File_Reader:
    def __init__(self, file_path):
        self.file_path = file_path

    def make_complex_from_file(self):
        simplex_dict = {}
        with open(self.file_path, 'r') as complex_file:
            
            for line in complex_file:
                vertex_string, weight = line.split(sep='; ')
                
                vertices = []
                for character in vertex_string.split(sep=','):
                    vertices.append(int(character))
                
                simplex = mWSC.weighted_simplex(vertices, int(weight))
                
                if len(vertices) - 1 in simplex_dict:
                    simplex_dict[len(vertices)-1].append(simplex)
                else:
                    simplex_dict[len(vertices)-1] = [simplex]
            return mWSC.mWSC(simplex_dict)

    def get_path(self):
        return self.file_path

    def set_path(self, new_path):
        self.file_path = new_path

def generate_complex_using_max_simplices(maximal_simplices, weight = 1): 
    """
    input: 
        maximal_simplices: list of lists of nonnegative integers

    output: 
        trivially weighted simplicial complex whose maximal simplices are 
        the input simplices

    This method is for quick construction of a simplicial complex for testing purposes by feeding 
    in the maximal simplices and generating all the lower dimensional simplices that must be 
    present by taking boundaries. 
    """
    # Work in order of highest dimension to lowest dimension - should avoid some redundant computation
    maximal_simplices.sort(key = lambda x: (-1)*(len(x)))
    simplex_dict = {} #for the construction of mWSC
    for simplex in maximal_simplices:
        #first add the current maximal simplex to the simplex dictionary
        if len(simplex)-1 in simplex_dict:
            simplex_dict[len(simplex)-1].append(weighted_simplex(simplex, weight))
        else:
            simplex_dict[len(simplex)-1] = [weighted_simplex(simplex, weight)]
        #recursively generate facets of simplex and add them    
        facets = generate_faces(simplex)
        for facet in facets:
            new_facet = weighted_simplex(facet, weight)
            if len(facet) - 1 in simplex_dict:
                if new_facet not in simplex_dict[len(facet) - 1]:
                    simplex_dict[len(facet) - 1].append(new_facet)
            else:
                simplex_dict[len(facet) - 1] = [new_facet]


    return mWSC.mWSC(simplex_dict)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Helper Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~#

def generate_faces(simplex):
    """
    input: 
        simplex: list of n + 1 nonnegative integers representing vertices of n-simplex

    output: 
        List of n-1 dimensional faces of simplex, each given as a list of n vertices
    """
    facets = []
    for vertex in simplex:

        face = [v for v in simplex if v != vertex] #this is the ith face map applied to the simplex

        if face != []: #this happens if the simplex was a vertex, and in this case there are no facets, 
                       #so the function will not update facets at all and return an empty list 
            facets.append(face) 

            lower_dim_facets = generate_faces(face) #recursively build the lower dimensional simplices as well, and then keep the ones we haven't seen before
            for item in lower_dim_facets:
                if item not in facets:
                    facets.append(item)

            
    return facets