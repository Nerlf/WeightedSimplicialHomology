import numpy as np 


"""
A weighted simplicial complex is a simplicial complex K together with a map 
v: K -> R with R a commutative ring. Currently we assume R = Q[[\pi]], which is  
a DVR with uniformizer \pi, and the weight of a simplex s is of the form 
v(s) = \pi^w(s) with w(s) a nonnegative integer. The weighted_simplex class 
is a container class holding the data of the underlying abstract simplex as an 
ordered list of vertices and the weight as a nonnegative integer. If no weight is 
supplied, the default value is 0, as this will make v(s) = 1. In the case 
that v(s) = 1 for all s, the weighted theory is equivalent to the classical 
theory of simplicial complexes. 

"""

class weighted_simplex:
    def __init__(self, vertex_list, weight):
        self.geometric_simplex = vertex_list
        self.weight = weight

    def dim(self):
        return len(self.geometric_simplex) - 1

    def get_weight(self):
        return self.weight

    def get_simplex(self):
        return self.geometric_simplex

    def geometric_boundary(self):
        faces = []
        for vertex in self.geometric_simplex:
            facet = [v for v in self.geometric_simplex if v != vertex]
            if facet != []:
                faces.append(facet)
        return faces
    
    def __str__(self):
        return f"({self.geometric_simplex}: weight is {self.weight})"

    def __repr__(self):
        return f"({self.geometric_simplex}: {self.weight})"

    def __eq__(self, other):
        my_underlying_simplex = self.get_simplex()
        other_underlying_simplex = other.get_simplex()
        other_weight = other.get_weight()
        simplex_equal = True 

        if len(my_underlying_simplex) == len(other_underlying_simplex):
            for i in range(len(my_underlying_simplex)):
                if my_underlying_simplex[i] != other_underlying_simplex[i]:
                    simplex_equal = False
        else:
            simplex_equal = False
        
        return simplex_equal and self.get_weight() == other_weight


    

class mWSC:
    def __init__(self, simplex_dict):
        self.simplex_lists = simplex_dict
        
        for dimension in self.simplex_lists:
            self.simplex_lists[dimension].sort(key = lambda x: -1*x.get_weight()) 
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
        self.boundaries = {}
        for dim in self.simplex_lists:
            self.boundaries[dim] = self.boundary_matrix(dim)
    
    def get_top_dimension(self):
        return len(self.simplex_lists) - 1
    
    def get_num_simplices(self, n):
        return len(self.simplex_lists[n])

    def get_simplices(self, n):
        if n > self.get_top_dimension():
            return []
        return self.simplex_lists[n]

    def get_boundary(self, dim): #TODO: needs exception
        return self.boundaries[dim]

    def boundary_matrix(self, n):
        """
        Calculates the boundary matrix from the group of n-chains to the 
        group of n-1 chains. Returns a numpy array representing 
        the boundary map with respect to the basis of n-simplices in the domain
        and the basis of n-1 simplices in the codomain - coefficients are taken 
        in a field, assumed at the moment to be the rationals.  
        """
        if n == 0:
            return np.zeros((1, self.get_num_simplices(0)))

        if n > self.get_top_dimension() or n <= -1:
            return np.array([0])

        n_simplices = self.get_simplices(n)
        target_simplices = self.get_simplices(n-1)
        
        num_cols = self.get_num_simplices(n)
        num_rows = self.get_num_simplices(n-1)
        boundary_matrix = np.zeros((num_rows, num_cols))
        for j, simplex in enumerate(n_simplices): #loop through the columns of boundary_matrix
            boundary = simplex.geometric_boundary()
            for i, face in enumerate(target_simplices):
                if face.get_simplex() in boundary:
                    ind = boundary.index(face.get_simplex())
                    boundary_matrix[i][j] = (-1)**ind
        return boundary_matrix

    def compute_K_basis(self, n):
        """
        Input: 
            n: non-negative integer 
        Output:
            M: list of ints
            K: list of ints 
            kappa_basis: list of lists of floats 
        """

        n_boundary = self.get_boundary(n) #do operations on the nth boundary matrix
        M = [] #indices go in here when we examine them and their simplex doesn't form a cycle with the other simplices already in M
        K = [] #simplices go in here if we examine them and they do form a cycle with the M simplices 
        kappa_basis = [] #I will put the coefficients necessary to form the kappa cycle in here for each kappa - i.e the solution from the least squares method


        """
        Case n = 0: In this case every vertex is a cycle in and of itself, so nothing gets added to M -
        in the general case M must be nonempty for the solver to function. Thus in the case n = 0,
        we return M = [], K  = range(#vertices), kappa_basis is returned as [[1], ..., [1]] and len(kappa_basis) = #vertices
        """
        if n == 0: 
            return (M, [i for i in range(self.get_num_simplices(0))], [[1] for simplex in self.get_simplices(0)])
        # Now we may assume that n > 0

        if n > self.get_top_dimension():
            return (M, K, kappa_basis)

        """
        We need to implement the step of algorithm 1 which 
        checks for a nontrivial set of coefficients on 
        the M_n simplices and the current simplex such that 
        we get a cycle; the procedure will be to check 
        whether Ax = 0  has a nontrivial solution, where 
        A is the columns of the boundary corresponding to 
        the M_n simplices and kappa. We can achieve this 
        through checking if the addition of the kappa column
        causes the rank of A to be deficient (if so, the 
        rank of A will necessarily be one less than the 
        number of columns). If there is no nontrivial 
        solution, then we add the current simplex to M_n. 
        Otherwise we solve A'y = boundary of current simplex 
        using the least squares method. 
        """
        M.append(0) #the first column will always be linearly independent - a simplex cannot be a cycle by itself in a valid simplicial complex
        for index in range(1, len(n_boundary[0])): #we examine each simplex in turn by looking at its corresponding column in the boundary matrix
            column_indices = M.copy()
            column_indices.append(index)
            A = n_boundary[:, column_indices]
            if np.linalg.matrix_rank(A) == len(column_indices):
                M.append(index)
            else:
                A_prime = A[:, 0:len(A[0]) - 1]
                b = A[:, len(A[0])-1]
                output_tuple = np.linalg.lstsq(A_prime, b, rcond=None)
                K.append(index)
                kappa_basis.append(output_tuple[0])
                #print(f"Error is: {output_tuple[1]}")

        return (M, K, kappa_basis) #return type of M, K are lists of integers - the integers represent the index of a simplex in the n+1 or n dimensional simplex list respectively

    def find_basis_pairing(self, dim, eps = 1, sweep = True):
        """
            input: 
                dim: int 
                eps: multiple of machine tolerance 
            output: 
                phi-coupling: list of 2-tuples representing pairings between kappa basis cycles and 
                n+1 boundary cycles. 
        """

        tol = eps * 10e-16
        
        # Fetch class variables needed for the calculation #
        n_simplex_list = self.get_simplices(dim)
        #num_n_simplices = self.get_num_simplices(dim) #use this number to walk backwards through n-simplices
        n_plus_1_simplex_list = self.get_simplices(dim+1)
        n_plus_1_boundary_matrix = self.get_boundary(dim+1)
        #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~#

        # Algorithm 2 in the write up begins here 
        dim_basis_output = self.compute_K_basis(dim)
        dim_plus_one_basis_output = self.compute_K_basis(dim+1)
        K_n = dim_basis_output[1]
        M_n_plus_1 = dim_plus_one_basis_output[0]

        
        """
        Construction of projection matrix Q: The columns of Q represent the cycles in the kappa basis
        represented by the simplices indexed by K_n. The rows of Q are the expressions of boundary_n+1(t_j)
        in terms of the kappa basis. 
        """
        Q = np.zeros((len(M_n_plus_1), len(K_n)))
        for i in range(len(M_n_plus_1)):
            boundary_tau_i = n_plus_1_boundary_matrix[:, M_n_plus_1[i]] #
            non_zero_indices = [k for k in range(len(boundary_tau_i)) if boundary_tau_i[k] != 0] #this gives the indices of n-simplices in the n-simplex list on which the boundary of tau_i is supported on
            for j in range(len(K_n)):
                for index in non_zero_indices:
                    if n_simplex_list[K_n[len(K_n)- j - 1]] == n_simplex_list[index]: #this walking backward means the columns correspond to the kappa basis in reverse order 
                        Q[i][j] = boundary_tau_i[index] 

        #TODO: Construction of Q can be parcelled into a helper function - may facilitate parallelization
        
        coupling_list = []

        for column in range(len(K_n)):
            min_index = -1
            row_view_index = 0

            for i in range(len(M_n_plus_1)):
                if Q[i][column] != 0:
                    min_index = i
                    break
                
            
            # If there is no minimum nonzero entry, then no elimination steps are required, 
            # so this should only execute if min_index was changed in the while loop above
            #print(f"Now the min index is: {min_index}")
            
            if min_index >= 0:
                # add pairing of kappa cycle to image cycle to phi-coupling 
                coupling_list.append((K_n[len(K_n) - column - 1], M_n_plus_1[min_index]))

                #TODO: This elimination step should be made into a helper function and multithreaded
                pivot_row = Q[min_index, :]
                for i in range(len(M_n_plus_1)):
                    
                    if i == min_index:
                        pass
                    else:
                        row_to_eliminate = Q[i, :]
                        pivot_elt = pivot_row[column]
                        kill_elt = row_to_eliminate[column]
                        row_to_eliminate = row_to_eliminate - (kill_elt/pivot_elt)*pivot_row
                        Q[i, :] = row_to_eliminate
               
                #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                #This last for loop sets every other entry of the pivot row to 0 except the pivot
                for j in range(len(pivot_row)):
                    if j == column:
                        continue
                    pivot_row[j] = 0.0
                if sweep:
                    zero_sweeper(Q, tol)    
        return coupling_list


    def get_Homology(self, n):
        M, kappa, B_kappa = self.compute_K_basis(n)
        torsion_coefficients = []
        if n == self.get_top_dimension():
            return (len(kappa), torsion_coefficients)
        phi_coupling = self.find_basis_pairing(n, sweep=False)
        n_dim_simplices = self.get_simplices(n)
        n_plus_1_dim_simplices = self.get_simplices(n+1)
        for couple in phi_coupling:
            torsion_coefficients.append(n_dim_simplices[couple[0]].get_weight()-n_plus_1_dim_simplices[couple[1]].get_weight()) #TODO: need polymorphic behavior based on base 
        if n == self.get_top_dimension():
            free_rank = len(kappa) - len(phi_coupling)
        else:
            M_n_plus_1, K_1, B_1 = self.compute_K_basis(n+1)
            free_rank = len(kappa) - len(M_n_plus_1)
        return (free_rank, torsion_coefficients)
    
    def print_Homology(self, n, field = 'Q'):
        if n > self.get_top_dimension() or n < 0:
            return "0"
        ring = "R"
        homology = self.get_Homology(n)
        torsion_submodule = ""
        print(homology) #DEBUG PRINT STATEMENT, REMOVE BEFORE NEXT COMMIT
        # This if needs checked - is it actually necessary to put the first torsion component in manually?
        if homology[1] != []:
            if homology[1][0] != 0:
                torsion_submodule = torsion_submodule + f" {ring}/(\u03C0^{homology[1][0]})"
            for i in range(1, len(homology[1])):
                if homology[1][i] == 0:
                    continue
                torsion_submodule = torsion_submodule + f"\u2295 {ring}/(\u03C0^{homology[1][i]})"
        # TODO: comments to explain control flow
        if torsion_submodule == " \u2295  ":
            torsion_submodule = ""
        return f"{ring}^{homology[0]} " + torsion_submodule



    # def print_Homology(self, n, field = 'Q'):
    #     ring = field + "[[x]]"
    #     homology = self.get_Homology(n)
    #     if homology[1][0] != 0:
    #         torsion_submodule = f"({ring}/x^{homology[1][0]})"
    #     else:
    #         torsion_submodule = ""
    #     for i in range(1, len(homology[1])):
    #         if homology[1][i] == 0:
    #             continue
    #         torsion_submodule = torsion_submodule + f"\u2295 {ring}/x^{homology[1][i]}"
    #     if torsion_submodule == "":
    #         return f"{ring}^{homology[0]}"
    #     else:
    #         return f"{ring}^{homology[0]} \u2295 " + torsion_submodule
            



#~~~~~~~~~~~~~~~~~ Some Helper Functions ~~~~~~~~~~~~~~~~~#

def generate_faces(simplex): #now a function of Simplicial_Constuctors
    """
    input: 
        simplex: list of n + 1 nonnegative integers representing vertices of n-simplex

    output: 
        List of n-1 dimensional faces of simplex, each given as a list of n vertices
    """
    facets = []
    for vertex in simplex:

        face = [v for v in simplex if v != vertex] #this is the ith face map applied to the simplex

        if face != []: #this happens if the simplex was a vertex, and in this case there are no facets, 
                       #so the function will not update facets at all and return an empty list 
            facets.append(face) 

            lower_dim_facets = generate_faces(face) #recursively build the lower dimensional simplices as well, and then keep the ones we haven't seen before
            for item in lower_dim_facets:
                if item not in facets:
                    facets.append(item)

            
    return facets

def geometric_order(simplex1, simplex2): #I don't think I use this function - this was going to be used for imposing a canonical order on the simplex lists
    """
    input: weighted_simplex simplex1 and simplex2
    output:  Boolean; True corresponds to simplex1 <= simplex2
    and False corresponds to simplex2 < simplex1
    This method compares the underlying geometric simplices of two weighted 
    simplices under the simplicial order. 

    If the two underlying geometric simplices are equal it will return True (simplex1 <= simplex2)
    by default. 
    """
    geo_simplex1 = simplex1.get_simplex()
    geo_simplex2 = simplex2.get_simplex()
    comparator = True

    for i in range(len(geo_simplex1)):
        if geo_simplex1[i] != geo_simplex2[i]:
            comparator = geo_simplex1[i] < geo_simplex2[i]
            break
    return comparator


def generate_complex_using_max_simplices(maximal_simplices): #now a method of Simplicial_Constructors
    """
    input: 
        maximal_simplices: list of lists of nonnegative integers

    output: 
        trivially weighted simplicial complex whose maximal simplices are 
        the input simplices

    This method is for quick construction of a simplicial complex for testing purposes by feeding 
    in the maximal simplices and generating all the lower dimensional simplices that must be 
    present by taking boundaries. 
    """
    # Work in order of highest dimension to lowest dimension - should avoid some redundant computation
    maximal_simplices.sort(key = lambda x: (-1)*(len(x)))
    simplex_dict = {} #for the construction of mWSC
    for simplex in maximal_simplices:
        #first add the current maximal simplex to the simplex dictionary
        if len(simplex)-1 in simplex_dict:
            simplex_dict[len(simplex)-1].append(weighted_simplex(simplex, 1))
        else:
            simplex_dict[len(simplex)-1] = [weighted_simplex(simplex, 1)]
        #recursively generate facets of simplex and add them    
        facets = generate_faces(simplex)
        for facet in facets:
            new_facet = weighted_simplex(facet, 1)
            if len(facet) - 1 in simplex_dict:
                if new_facet not in simplex_dict[len(facet) - 1]:
                    simplex_dict[len(facet) - 1].append(new_facet)
            else:
                simplex_dict[len(facet) - 1] = [new_facet]


    return mWSC(simplex_dict)

def zero_sweeper(matrix, precision):
    """
        Input: n x m numpy array
        Output: None 

        Sweeps through the array to set any values that are smaller in absolute 
        value than the specified precision value. 
    """
    for i in range(len(matrix)):
        for j in range(len(matrix[0])):
            if abs(matrix[i][j]) < precision:
                matrix[i][j] = 0
    return 0



class InvalidSCError(Exception):
    """
    Exception for attempting to make an invalid monotone
    weighted simplicial complex, or attempting to do an illegal 
    move on a simplicial complex
    """ 

    def __init__(self, message):
        #uses constructor of the Exception class
        super().__init__(message)
    