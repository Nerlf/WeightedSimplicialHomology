from WeightedSimplicialHomology import mWSC as mWSC
from WeightedSimplicialHomology.Simplicial_Constructors import Simplicial_File_Reader
import pickle

test_reader = Simplicial_File_Reader("weighted_joined_triangles.txt")

triangles_complex = test_reader.make_complex_from_file()

print("~~~~~~~~~~~~~~ Joined Triangles Complex ~~~~~~~~~~~~~~")
print(triangles_complex.get_simplices(0))
print(triangles_complex.get_simplices(1))
print(triangles_complex.get_simplices(2))
print("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~")

# with open("sphere.simp", 'rb') as sphereRead:
#     sphere = pickle.load(sphereRead)
#     print(sphere.get_simplices(1))
print("~~~~~~~~~~~~~~~~~~~~~~~ Joined Triangles Homology ~~~~~~~~~~~~~~~~~~~~~~~~~~")
print(triangles_complex.get_Homology(0))
print(triangles_complex.get_Homology(1))
print(triangles_complex.get_Homology(2))
