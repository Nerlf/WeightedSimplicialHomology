import sys 
import numpy
from WeightedSimplicialHomology import mWSC as mWSC
import pickle

torus = mWSC.generate_complex_using_max_simplices([
[0,1,3],
[1,3,5],
[1,5,2],
[5,2,6],
[2,6,0],
[6,0,3],
[3,4,5],
[4,5,7],
[5,7,6],
[7,6,8],
[6,8,3],
[8,3,4],
[4,0,7],
[0,7,1],
[7,1,8],
[1,8,2],
[8,2,4],
[2,4,0]
])

sphere = mWSC.generate_complex_using_max_simplices([
    [0,1,2],
    [0,2,3],
    [1,2,3],
    [0,1,3]
]) 

rp2 = mWSC.generate_complex_using_max_simplices([
    [0,1,8],
    [0,8,7],
    [0,5,7],
    [5,4,7],
    [4,6,7],
    [6,8,7],
    [3,4,6],
    [2,1,8],
    [2,6,8],
    [3,2,6]
])

  

moebius_strip = mWSC.generate_complex_using_max_simplices([[0,1,2],[0,2,3],[2,3,4], [3,4,5],[0,4,5],[0,1,5]])


torus_write = open('torus.simp', 'wb')

strip_write = open('mobius_strip.simp', 'wb')

sphere_write = open('sphere.simp', 'wb')
print("Writing RP2")
rp_write = open('rp2.simp', 'wb')

print("Writing Torus")
pickle.dump(torus, torus_write)
print("Writing Mobius Strip")
pickle.dump(moebius_strip, strip_write)
print("Writing Sphere")
pickle.dump(sphere, sphere_write)
print("Writing RP2")
pickle.dump(rp2, rp_write)

torus_write.close()
strip_write.close()
sphere_write.close()
rp_write.close()