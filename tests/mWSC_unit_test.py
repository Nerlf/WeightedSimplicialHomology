from WeightedSimplicialHomology.mWSC import *
from WeightedSimplicialHomology import Simplicial_Constructors
import numpy as np
import unittest
import pickle

class BoundaryTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_Boundary_of_a_2_simplex(self):
        """
        This test will take the boundary of a standard 2-simplex [0,1,2] - the 
        returned matrix should be [[1], [-1], [1]], which represents [1,2] - [0,2] + [0,1]. 
        """
        simplex_dict = {
            0: [mWSC.weighted_simplex([0], 1), mWSC.weighted_simplex([1], 1), mWSC.weighted_simplex([2], 1)],
            1: [mWSC.weighted_simplex([1,2],1), mWSC.weighted_simplex([0,2], 1), mWSC.weighted_simplex([0,1], 1)],
            2: [mWSC.weighted_simplex([0,1,2], 1)]
        }
        standard_2_simplex = mWSC.mWSC(simplex_dict)
        A = standard_2_simplex.boundary_matrix(2)
        self.assertTrue(np.array_equal(A, np.array([[1], [-1], [1]])))

    def test_Dimensions_above_top_dimension_have_boundary_0(self):
        """
        This is a basic test to ensure that if boundary_matrix gets called with
        a dimension outside the range of dimensions in the complex then a 
        zero matrix is returned. 
        """
        point_complex = mWSC.mWSC({0: [mWSC.weighted_simplex([0], 1)]})
        A = point_complex.boundary_matrix(-1)
        B = point_complex.boundary_matrix(-2)
        C = point_complex.boundary_matrix(1)
        self.assertTrue(np.array_equal(A, [0]))
        self.assertTrue(np.array_equal(B, [0]))
        self.assertTrue(np.array_equal(C, [0]))
    
    def test_Boundary_0_is_0(self):
        """
        The zero dimensional boundary should be a 
        1 x #vertices zero matrix, for homological considerations. 
        """
        vertex_complex = mWSC({
            0: [weighted_simplex([0], 1), weighted_simplex([1], 1), weighted_simplex([2], 1), weighted_simplex([3], 1)] 
        })
        A = vertex_complex.boundary_matrix(0)
        self.assertTrue(np.array_equal(A, [[0,0,0,0]]))

class compute_K_basisTest(unittest.TestCase):
    def setUp(self):
        pass

    def test_K_basis_of_a_standard_2_simplex(self):
        standard_2_simplex = generate_complex_using_max_simplices([[0,1,2]])
        M2, K2, kappa2 = standard_2_simplex.compute_K_basis(2)
        M1, K1, kappa1 = standard_2_simplex.compute_K_basis(1)
        M0, K0, kappa0 = standard_2_simplex.compute_K_basis(0)

        self.assertTrue(len(M2) == 1 and len(K2) == 0)
        self.assertTrue(len(K1) == 1 and K1[0] == 2)
        print(M1)
        self.assertTrue(K1[0] == 2 and len(M1) == 2)
        print(K0)
        self.assertTrue(len(M0) == 0 and len(K0) == 3)

    def test_K_basis_of_bridged_triangles(self):
        bridged_triangles =  Simplicial_Constructors.generate_complex_using_max_simplices([[0,1,2],[3, 4, 5], [2,3]])
        M2, K2, kappa2 = bridged_triangles.compute_K_basis(2)
        M1, K1, kappa1 = bridged_triangles.compute_K_basis(1)
        self.assertTrue(M2[0] == 0 and M2[1] == 1)
        self.assertTrue(K1[0] == 2 and K1[1] == 5)
    
    def test_K_basis_of_sphere(self):
        sphere =  Simplicial_Constructors.generate_complex_using_max_simplices([[0,1,2], [0,2,3], [0,1,3], [1,2,3]])
        M, K, kappa = sphere.compute_K_basis(2)
        two_simplices = sphere.get_simplices(2)
        self.assertTrue(two_simplices[K[0]] == weighted_simplex([1,2,3], 1))
    
    def test_K_basis_of_the_torus(self):
        torus_read = open('torus.simp', 'rb')
        torus = pickle.load(torus_read)
        M, K, kappa = torus.compute_K_basis(2)
        print(len(M))
        print(len(K))
        self.assertTrue(len(K) == 1)
    
    def test_K_basis_of_RP2(self):
        rp2_read = open('rp2.simp', 'rb')
        rp2 = pickle.load(rp2_read)
        M, K, kappa = rp2.compute_K_basis(2)
        M1, K1, kappa1 = rp2.compute_K_basis(1)
        print(K1)
        self.assertTrue(len(K) == 0)
        self.assertTrue(len(K1) == 10)


if __name__ == '__main__':
    unittest.main()
