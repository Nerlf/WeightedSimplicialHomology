import numpy as np
import pickle 
from mWSC import *

# torus_read = open("torus.simp", "rb")
# torus = pickle.load(torus_read)

# sphere = generate_complex_using_max_simplices([[0,1,2], [0,2,3], [0,1,3], [1,2,3]])



# print(torus.get_num_simplices(2))

# print(torus.get_simplices(2))

# A = torus.get_boundary(2)

# B = sphere.get_boundary(2)

#print(A)

#print(np.linalg.matrix_rank(B))

# vec = np.zeros((torus.get_num_simplices(2), 1))

# for i in range(torus.get_num_simplices(2)):
#     vec[i][0] = (-1)**i

#print(np.matmul(A, vec))


# better_torus = generate_complex_using_max_simplices([
# [0,1,3],
# [1,3,5],
# [1,5,2],
# [5,2,6],
# [2,6,0],
# [6,0,3],
# [3,4,5],
# [4,5,7],
# [5,7,6],
# [7,6,8],
# [6,8,3],
# [8,3,4],
# [4,0,7],
# [0,7,1],
# [7,1,8],
# [1,8,2],
# [8,2,4],
# [2,4,0]
# ])

my_torus = generate_complex_using_max_simplices([
[5,0,6],
[0,6,2],
[6,2,7],
[2,7,4],
[7,4,5],
[4,5,0],
[0,1,2],
[1,2,8],
[2,8,4],
[8,4,3],
[4,3,0],
[3,0,1],
[1,5,8],
[5,8,6],
[8,6,3],
[6,3,7],
[3,7,1],
[7,1,5]
])

# M, K, kappa = better_torus.compute_K_basis(2)

#print(K[0])

M1, K1, kappa1 = my_torus.compute_K_basis(2)

print(K1[0])