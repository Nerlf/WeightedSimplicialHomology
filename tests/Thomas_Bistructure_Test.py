import os, mWSC, time
from WeightedSimplicialHomology.Simplicial_Constructors import Simplicial_File_Reader

complex_generator = Simplicial_File_Reader("weighted_joined_triangles.txt")
test_complex = complex_generator.make_complex_from_file()

overall_start = time.time()

with os.scandir("./biSec") as file_scanner:
    for case in file_scanner:
        case_start = time.time()
        if case.name.endswith(".txt"):
            complex_generator.set_path(case)
            test_complex = complex_generator.make_complex_from_file()
            print(f"~~~~ Homology for {case.name} ~~~~")
            dim = test_complex.get_top_dimension()
            for i in range(dim + 1):
                ith_homology = test_complex.get_Homology(i)
                if i == 0:
                    num_vertices = test_complex.get_num_simplices(0)
                print(f"H_{i}: Rank {ith_homology[0]}, torsion exponents {ith_homology[1]}")
        case_end = time.time()
        print(f"{case.name} took {case_end - case_start} s")
overall_end = time.time()

print(f"Total runtime is {overall_end - overall_start} s")