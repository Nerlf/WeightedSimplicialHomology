import pickle
import numpy
from WeightedSimplicialHomology import mWSC as mWSC
import time

start = time.time()
sphere_reader = open('sphere.simp', 'rb')
torus_reader = open('torus.simp', 'rb')
proj_plane_reader = open('rp2.simp', 'rb')
torus = pickle.load(torus_reader)
sphere = pickle.load(sphere_reader)
rp2 = pickle.load(proj_plane_reader)
# print(sphere.get_Homology(1))
# print(torus.get_Homology(1))


print("~~~~~~~~~~~~ 0th homology~~~~~~~~~~~~~~")
print(sphere.print_Homology(0))
print(torus.print_Homology(0))
print(rp2.print_Homology(0))

print("~~~~~~~~~~~~~~1st Homology~~~~~~~~~~~~")
print(sphere.print_Homology(1))
print(torus.print_Homology(1))
print(rp2.print_Homology(1))

print("~~~~~~~~~~~~~~2nd Homology~~~~~~~~~~~~")
print(sphere.get_Homology(2))
print(torus.get_Homology(2))
print(rp2.get_Homology(2))

end = time.time()

print(f"We took: {end - start}")