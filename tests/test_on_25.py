import os, mWSC, time
from Simplicial_File_Reader import Simplicial_File_Reader

complex_generator = Simplicial_File_Reader("./biSec/25.txt")
test_complex = complex_generator.make_complex_from_file()

zeroth_homology = test_complex.get_Homology(0)
# print("Boundary matrix is:")
# print(test_complex.get_boundary(1))
M_n_plus_1, K1, kappa1 = test_complex.compute_K_basis(1)

print(zeroth_homology)

#print(f"H_0: Rank {zeroth_homology[0]}, torsion coefficients {zeroth_homology[1]}")

print(test_complex.print_Homology(0))   
